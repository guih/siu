var async    = require('async');
var auth     = require('./lib/auth');
var score    = require('./lib/score');
var schedule = require('./lib/schedule');
var tmp      = {};
var student  = {};


async.waterfall([
    signAuth,
    fetchScore,
    fetchSchedule,
], function (err, result) {
    console.log('+++++++++++++++++=');
});

function signAuth(callback) {
    function afterSignAuth(session){
        callback(null, session);
    }
    auth.login({user: '71352', pass: 'familiac1'}, afterSignAuth);
}
function fetchScore(session, callback) {
    score.get( session, function(score){
        tmp.score = score;
        callback(null, session);
    } );
}
function fetchSchedule(session, callback) {
    schedule.get(session, function(scheduleData){
        // tmp.schedule = scheduleData;
        callback(null, scheduleData);
    });
}