// var cheerio       = require('cheerio');
// var request       = require('request');
// var static        = require('./static');
// var target        = static.target;
// var mainTarget    = static.mainTarget;
// var mainUserAgent = static.ua;

// function getSchedule(args, fn){    
//     var cookie = args.token;
//     var user   = args.user;
    

//     var headers = {
//         'User-Agent': 'Mozilla/5.0',
//         'Accept': 'text/html',
//         'Referer': mainTarget + 'Curso.aspx?M='+user,
//         'Cookie': cookie 
//     };  

//     var options = {
//         url: mainTarget + 'HorarioAulas/Horario.aspx',
//         headers: headers
//     };

//     function onPageFetched(error, response, body) {
//         if (!error && response.statusCode == 200) {
//             var e = [];
//             var scheduleTable = cheerio.load(body);
//             console.log(body);
//         }
//     }

//     request(options, onPageFetched);
// }

// exports.get = getSchedule;



var cheerio       = require('cheerio');
var request       = require('request');
var static        = require('./static');
var helpers       = require('./helpers');
var target        = static.target;
var mainTarget    = static.mainTarget;
var mainUserAgent = static.ua;

/**
 * Request schedule data
 * @constructor
 * @param {Obect} args - list of parameters requried to perform a clear http request
 * @param {function} fn - the callback funcion called with request parsed body
 */
function getSchedule(args, fn){    

    // set variables that will recognize the current user
    var cookie = args.token;
    var user   = args.user;


    // carefull here, if given data is invalid it will throw an exception
    var kwargs = { 
        path: 'HorarioAulas/Horario.aspx', 
        user : user, 
        session: cookie
    };
    
    // make http call to target
    helpers.http.request(kwargs, parseReturnedData);

    function parseReturnedData(body){
        var matrix    = [];
        var $         = cheerio.load(body);
        var lines     = $('#ctl00_ContentPlaceHolder1_TabContainer1_TabPanel1_grdMatutino').children();
        var interator = lines.find('th').length;
        
        // console.log(scheduleDOM(lines[1]).find('tr').text())

        // console.log($(lines[0]).find('tr').text());
        // 
        //console.log(lines);
        // return;
        
        //lines.each(function(index, val){
            console.log( "Starting days: \n\n\n" );
            $( lines[0] ).find( 'th' ).each( function( i,td ) {
                //if( i < 3 ) return;
                if( i < 2 ) return;
                console.log('\n\n\n\n\n\n');
                console.log( $(td).text() );
                lines.each(function(index, hour){
                    if( index == 0 ) return;

                    //console.log( $( $( hour ).find( 'td' )[3] ).text() );
                    console.log( $( $( hour ).find('> td')[i] ).find('a').text() );
                });
            });

            /*return;
            if( index == 0 ) return;


            $(val).find('td').each(function(i, td){
                if( i !== 3 ) return;

                console.log(td, ')))))))))))))))0')
            });*/
            // console.log({
            //     detailt: $($(val).find('tr').find('td')[5]).text().trim().split('\r\n').map(function(str){
            //         return str.trim();
            //     })
            // });

            // console.log({
            //     day: scheduleDOM(lines.find('th')[3]).text(),
            //     hour: scheduleDOM(scheduleDOM(val).find('td')[0]).text().trim(),
            //     hourAlt: scheduleDOM(scheduleDOM(val).find('td')[1]).text().trim().split('\r\n').map(function(str){
            //         return str.trim();
            //     }),
            //     detailt: scheduleDOM(scheduleDOM(val).find('tr')[3]).text().trim().split('\r\n').map(function(str){
            //         return str.trim();
            //     })
            // });

            // console.log(index ,'>>>>>>>>>', scheduleDOM(val).find('tr').text().split('\r\n').map(function(str){
            //          return str.trim();
            //    }))

            // matrix.push();
            // matrix.push();
            // 
            





            // for( var i =0; i<interator; i++ ){
            //     var line = scheduleDOM(scheduleDOM(val).find('td')[i]).text().trim();
            //     if( line != '' && scheduleDOM( lines.find('th')[i] ).text() != '' )
            //         matrix.push( { day: scheduleDOM( lines.find('th')[i] ).text(), course: line} );
            // }

            // if( index == lines.length - 1 )
                // console.log(matrix);        
        //});
        // console.log(lines, lines.length);
    }
}

exports.get = getSchedule;