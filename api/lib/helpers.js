var static        = require('./static');
var request        = require('request');
var target        = static.target;
var mainTarget    = static.mainTarget;
var mainUserAgent = static.ua;

function authenticatedRequest(args, fn){

	var path    = args.path;
	var user    = args.user;
	var session = args.session;


    var headers = {
        'User-Agent': 'Mozilla/5.0',
        'Accept': 'text/html',
        'Referer': mainTarget + 'Curso.aspx?',
        'Cookie': session 
    };  

    var options = {
        url: mainTarget + path +'?M='+user,
        headers: headers
    };

    function onPageFetched(error, response, body) {
    	// console.log(body);
        if (!error && response.statusCode == 200) {
        	fn(body);
        }
    }

    request(options, onPageFetched);	
};


module.exports = {
	http: {
		request: authenticatedRequest
	},
	string: {

	},
	url: {

	}
};