var request       = require('request');
var cheerio       = require('cheerio');
var colors        = require('colors');
var util          = require('util');
var url           = require('url');
var static        = require('./static');
var target        = static.target;
var mainTarget    = static.mainTarget;
var mainUserAgent = static.ua;


function fetchForm(args, fn) {
    var form = [],
    name,
    value, $
    target = mainTarget + 'login.aspx';

    if( !args || args.user == "" || args.user == undefined ){
        console.log('No user!'.red.bold);
        return;
    }

    if( !args || args.pass == "" || args.pass == undefined ){
        console.log('No pass!'.red.bold);
        return;
    }


    console.log('getting form...'.bold)

    request.get({
        url: target
    }, afterGetForm);

    function afterGetForm(error, response, body) {
        $ = cheerio.load(body);

        $('#aspnetForm').find('input').each(buildQuery);

        function buildQuery(index, val) {
            name = $(val).attr('name');
            value = $(val).val();

            if ( name == 'ctl00$ContentPlaceHolder1$txtSenha' ){
                form.push({
                    name: name,
                    val: args.pass
                });
                return;
            }

            if ( name == 'ctl00$ContentPlaceHolder1$txtMatricula' ){
                form.push({
                    name: name,
                    val: args.user
                });
                return;
            }

            form.push({
                name: name,
                val: value
            });

            if (index == $('#aspnetForm').find('input').length - 1) {
                form.pop();
                console.log('form fetched! parsing...'.bold)

                parseFields({
                    fields: form
                }, function(dataString) {
                    fn(dataString);
                });
            }
        }
    }
}

function parseFields(form, fn) {
    var dataString = "",
        fields     = form.fields,
        fieldCount = fields.length,
        field;

    for (i in fields) {
        field = fields[i];

        dataString += util.format( '%s=%s&', 
            encodeURIComponent(field.name), 
            encodeURIComponent(field.val) );

        if (i == fieldCount - 1) 
            fn(dataString);
    }
}


function login(args, fn) {
    fetchForm(args, processRequest);

    function processRequest(dataString) {

       var applyTo = target;

       function onUserLogin(error, res, body) {
            if (error && res.statusCode !== 200 || res.statusCode !== 302) {
                console.log('HTTP ERROR');
            }

            var uCookie   = util.format( '%s;', res.headers['set-cookie'][0].split(';')[0] );
            var myCookie  = request.cookie(res.headers['set-cookie'][0]);
            var cookieJar = request.jar();
            var $         = null;    
            var userId    = null;            

            cookieJar.setCookie(myCookie, applyTo);
            
            $      = cheerio.load(body);
            userId = url.parse( decodeURIComponent( $('a').attr('href') ) ).search;

            if( !userId ){
                console.log('Error: Auth failed. Check user or pass.');
                return;
            }


            if( userId )
                if( userId.indexOf('=') !== -1 )
                    userId = userId.split('=')[1];

            console.log('user %s logged in', userId);

            exchangeCookieAuth({jar: cookieJar, str: uCookie}, userId, function(cookie){
                fn(cookie);
            });
       }

        request({
            url: mainTarget + 'login.aspx',
            method: 'POST',
            headers: {
                'Origin': target,
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Accept': '*/*',
                'Referer': mainTarget + '',
            },
            body: dataString.slice(0, -1)
        }, onUserLogin);
    }
}


function exchangeCookieAuth(cookie, user, fn){

    var target = mainTarget + 'Curso.aspx?M=' + user;

    function onExchangeCookie(err, res, body) {
        if (err) { return console.log(err) };

        var $ = cheerio.load(body),
        name,
        value
        form = [],
        afterDataStr = "";

        // build Exchange cookie query (seriously?)
        function buildExchangeQuery(index, val) {
            name = $(val).attr('name');
            value = $(val).val();

            form.push({
                name: name,
                val: value
            });

            if (index == $('#aspnetForm').find('input').length - 1) {
                console.log('Exchange data get, working...')

                runtimeInputs = [];

                form.push({
                        name: '__ASYNCPOST',
                        val: true
                    }, {
                        name: '__EVENTTARGET',
                        val: 'ctl00$ContentPlaceHolder1$grdCursos$ctl02$lkbDescCurso'
                    }, {
                        name: 'ctl00$ScriptManager1',
                        val: 'ctl00$UpdatePanel1|ctl00$ContentPlaceHolder1$grdCursos$ctl02$lkbDescCurso'
                });

                for (index in form) {
                    formField = form[index];

                    afterDataStr += encodeURIComponent(formField.name) +
                        '=' + encodeURIComponent(formField.val) + '&';


                    if (index == form.length - 1) {
                        var headers = {
                            'Origin': target,
                            'User-Agent': mainUserAgent,
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                            'Accept': '*/*',
                            'Cache-Control': 'no-cache',
                            'Referer': mainTarget + 'Curso.aspx?M='+user,

                            'Cookie': cookie.str + ' __utmt=1; __utma=124545928.1366971266.1472001473.1472001473.1472001473.1; __utmb=124545928.24.10.1472001473; __utmc=124545928; __utmz=124545928.1472001473.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
                        };

                        var options = {
                            url: mainTarget + 'Curso.aspx?M='+user,
                            method: 'POST',
                            headers: headers,
                            body: afterDataStr
                        };

                        function callback(error, response, body) {
                            if (!error && response.statusCode == 200) {
                                console.log('Evrrything is okay til now... Hope it will work!');
                                fn( { token: cookie.str, user: user });
                            }
                        }

                        request(options, callback);
                    }
                }
            }
        }

        $('#aspnetForm').find('input').each(buildExchangeQuery);
    };

    request({
        uri: target,
        jar: cookie.jar,
        headers: { 'Host': 'www.siu.univale.br' }
    }, onExchangeCookie);
}


exports.login = login;

// login({user: '71352', pass: 'familiac1'});