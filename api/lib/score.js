var cheerio       = require('cheerio');
var request       = require('request');
var static        = require('./static');
var helpers        = require('./helpers');
var target        = static.target;
var mainTarget    = static.mainTarget;
var mainUserAgent = static.ua;

/**
 * Request score data
 * @constructor
 * @param {Obect} args - list of parameters requried to perform a clear http request
 * @param {function} fn - the callback funcion called with request parsed body
 */
function getScore(args, fn){    

    // set variables that will recognize the current user
    var cookie = args.token;
    var user   = args.user;


    // carefull here, if given data is invalid it will throw an exception
    var kwargs = { 
        path: 'Default.aspx', 
        user : user, 
        session: cookie
    };
    
    // make http call to target
    helpers.http.request(kwargs, parseReturnedData);

    function parseReturnedData(body){
        var matrix       = [];
        var scoreTableId = '#ctl00_ContentPlaceHolder1_DisciplinasAluno1_grdDisciplinasEmCurso';
        var scoreDOM     = cheerio.load(body);
        
        var table$el     = scoreDOM(scoreTableId);
        var target$el    = table$el.find('tr');

        target$el.each(markupFilter);

        function markupFilter(index, val){
            var discipline = scoreDOM(val).find('td')[0];
            var totalScore = scoreDOM(val).find('td')[1];

            // clear whitespace rows
            if( scoreDOM(discipline).text().trim() !== '' ){                                                
                matrix.push({
                    matter: scoreDOM(discipline).text().trim(),
                    score: scoreDOM(totalScore).text().trim()
                });
            }

            if( index == target$el.length - 1)
                fn(matrix);
        }
    }
}

exports.get = getScore;